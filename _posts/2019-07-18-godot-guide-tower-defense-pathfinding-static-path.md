---
title: "Godot Guide: Tower Defense Pathing with Static Paths"
picture: "/assets/images/simple-tower-defense-pathfinding/final.gif"
---

There are a few different types of pathfinding in tower defense games. Today we'll be talking about the most simple variety, static paths. In other words, each level of your game would have one pre-defined path for creeps to go down and nothing will interfere with that path. This is the type of system used in games like Bloons TD, (find some other examples)

# Part 1: Setup

If you're just looking for the final project, there's a public git repository [here.](https://gitlab.com/DarwinGiles/godot-tutorial-tower-defense-static-pathfinding)

If you're looking to build things yourself, however, you'll need some art assets for the level and your creeps. I'm going to be using a tower defense tileset created by Kenney, which you can download for free [here.](https://www.kenney.nl/assets/tower-defense-top-down)

Before we begin, you should be aware of what knowledge you're expected to bring to class today. This isn't a particularly advanced lesson, but it's not a beginner tutorial either. So you'll want to make sure you're comfortable with the following:

* Performing basic tasks in the Godot Engine (eg placing nodes, creating scripts, editing properties, etc.)
* Reading and writing gdscript syntax
* Making tileset and map from a tilesheet with autotiling.
* How to clip a sprite out of a sprite sheet using the Region properties.

# Part 2: Plan

Before we start coding, it's important to have a solid grasp of what we're actually going for. We should be aware of what assumptions we're making and we want to know what we're ignoring. These are our assumptions and constraints.

We'll start with the base idea in our heads "We want to get the enemies in our tower defense game moving properly."

What assumptions are contained in that statement? Well there's the immediate acceptance that we have enemies in our game, and by assuming that it's a tower defense game, that implies we'll be building static fortifications against those enemies. Actually that implies a lot of stuff, most of which we don't need for this tutorial.

The important assumption is the idea of getting our enemies moving "Properly". In the context of a tower defense game, there is more than one way to do movement and it varies from game to game. In this tutorial, however, we're doing the absolute basics. 
* We want a single, static path that every enemy will walk down. 
* We want the enemy to face the direction they're currently walking. 
* We'll add a little spice on top by saying that we want enemies to spread out along the width of the path so they're not all moving in single file
Any complexity past that is beyond the scope of our work today.

Now as for constraints, we're going to ignore pretty much everything else. We don't care about collision or hit detection for the creeps. We don't care how they're going to die or what happens when they reach the end of the road. (Though we will delete them when they do.)

The only thing we're doing that could be considered "extra" is going through the trouble of building an actual road graphic with the tileset I linked above. Technically speaking, we could just make the creeps walk through the void of space in whatever shape we wanted (and for prototyping, it might be better to have them do so before bothering to build a map for them) but graphics are nice and making your game nice to look at makes it more fun to work on. So we're going to have a path and we're going to have some actual sprites.

Part 3: Programming

Before we do anything, let's build a map out of the tilesheet. You'll want to make an autotile for the path and one tile for the non-path area. The tilesheet offers various different color options, and you can pick whichever you want. I'll be using the tan path with the gray background.

Make sure your path begins and ends outside of the boundaries of the screen.

![Map Picture](/assets/images/simple-tower-defense-pathfinding/map.png)

Next, you'll want to create a Path2D node, with points that follow the path you've laid out on your map. Don't worry about curves just yet, just get the points set out and we'll curve it and make it nice a bit later.

![Path Picture](/assets/images/simple-tower-defense-pathfinding/path2d.png)

Next, you'll want to create a new scene. This will be your creep. Make the base node a PathFollow2D node, and add a sprite node. Use the sprite's texture region tools to cut out one of the appropriate sprites from the tilesheet.

![Creep](/assets/images/simple-tower-defense-pathfinding/creep.png)

Next, add a built-in script to the PathFollow2D node. In this script, we'll write a simple bit of code to get the creep moving down the path from one end to the other.

![Moving along the path](/assets/images/simple-tower-defense-pathfinding/move-on-path.png)

Next, we'll add another piece of code to our script to make the creep remove itself when it reaches the end of the path. (This is where you would call any code to remove a life or end the game when you're building in that functionality.)

![Kill the creep](/assets/images/simple-tower-defense-pathfinding/kill-at-end-of-path.png)

Finally, put your creep on your map scene, make it a child of the Path2D, and watch it go.

![Walking gif](/assets/images/simple-tower-defense-pathfinding/path-walking.gif)

This is good, but let's give him some friends. Add a built-in script to the Path2D. Then add a Timer node as a child of your Path2D. Set the timer to autostart. Then connect the timeout signal on the timer to the Path2D node. Then, add the following code and you'll have a number of creeps popping onto your path.

![Spawning gif](/assets/images/simple-tower-defense-pathfinding/creep-spawning.png)

We could stop here, but let's add a nice little extra polish. First, we'll curve the path. Not only does this smooth out the turns the creeps make, but it's required to create the effect we're about to add.

![Curving path](/assets/images/simple-tower-defense-pathfinding/curve-the-path.png)

We want our creeps to spread out along the width of the path. To do this, we're going to simply offset the creep's sprite position from its base PathFollow2D node. Make sure that if you add collision, to also make the creep's hitbox be offset.

To get this offset, add the following code to your creep's script.

![Offset script](/assets/images/simple-tower-defense-pathfinding/creep-offset.png)

With that, we're done. If you've followed along successfully, you should have something that looks like the following.

![Final result](/assets/images/simple-tower-defense-pathfinding/final.gif)

If you want to give me feedback, ask for help, or complain about how wrong I am, you can find me on twitter [here.](https://twitter.com/EvilDarwinGiles)
